import React, { Component } from "react";
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
} from "react-native";
TouchableOpacity.defaultProps = { activeOpacity: 0.7 };

import { createStackNavigator } from "@react-navigation/stack";

const Stack = createStackNavigator();

export default class Onbroading extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <SafeAreaView>
        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
            height: "80%",
          }}
        >
          <View>
            <Image
              source={require("../../assets/teamwork.png")}
              style={{ width: 300, height: 300 }}
            ></Image>
          </View>
        </View>
        <View
          style={{
            alignItems: "center",
            justifyContent: "flex-start",
            height: "20%",
          }}
        >
          <TouchableOpacity
            style={styles.appButtonContainer}
            onPress={() => {
              this.gotoHome();
            }}
          >
            <Text style={styles.appButtonText}>Sign Up</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              this.gotoSingIn();
            }}
          >
            <Text style={styles.subBtnTxt}>Have an account ?</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }

  gotoHome = () => {
    this.props.navigation.navigate({ name: "Home" });
  };

  gotoSingIn = () => {
    alert("Dahyun");
  };
}

const styles = StyleSheet.create({
  appButtonContainer: {
    backgroundColor: "#ff5400",
    borderRadius: 20,
    width: 300,
    paddingVertical: 10,
    paddingHorizontal: 12,
  },
  appButtonText: {
    fontSize: 20,
    color: "#fff",
    alignSelf: "center",
  },
  subBtnTxt: {
    marginTop: 20,
    fontSize: 14,
    color: "#343a40",
  },
});
