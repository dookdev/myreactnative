import React, { Component } from "react";
import { StyleSheet, Text, View, Image, StatusBar } from "react-native";
import AppIntroSlider from "react-native-app-intro-slider";

const slides = [
  {
    key: "1",
    title: "Team Work",
    text: "Description.\nSay something cool",
    image: require("../../assets/teamwork.png"),
    backgroundColor: "#59b2ab",
  },
  {
    key: "2",
    title: "Security",
    text: "Other cool stuff",
    image: require("../../assets/id-card.png"),
    backgroundColor: "#febe29",
  },
  {
    key: "3",
    title: "Report",
    text: "I'm already out of descriptions\n\nLorem ipsum bla bla bla",
    image: require("../../assets/bar-chart.png"),
    backgroundColor: "#22bcb5",
  },
];

export default class Intro extends Component {
  constructor(props) {
    super(props);
  }
  state = {
    showRealApp: false,
  };

  _renderItem = ({ item }) => {
    return (
      <View
        style={[
          styles.slide,
          {
            backgroundColor: item.backgroundColor,
          },
        ]}
      >
        <Text style={styles.title}>{item.title}</Text>
        <Image source={item.image} style={styles.image} />
        <Text style={styles.text}>{item.text}</Text>
      </View>
    );
  };

  _onDone = () => {
    this.setState({ showRealApp: true });
    this.props.navigation.navigate({ name: "Onbroad" });
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar translucent backgroundColor="transparent" />
        <AppIntroSlider
          renderItem={this._renderItem}
          data={slides}
          onDone={this._onDone}
        ></AppIntroSlider>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffb5a7",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  title: {
    fontSize: 22,
    color: "white",
    textAlign: "center",
  },
  text: {
    color: "rgba(255, 255, 255, 0.8)",
    textAlign: "center",
  },
  slide: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "blue",
  },
  image: {
    width: 280,
    height: 280,
    marginVertical: 32,
  },
});
